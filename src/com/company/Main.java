package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        String text = "ala ma kota, kot koduje w Javie kota";
        text = "Kto jak kto, ale kto Ale lubi ten pewnie jest Aliny kot!!!";

        Map indexMap = index(text);
        printMap(indexMap);
    }

    private static Map index(String in){

        String[] words = in.toLowerCase().replaceAll("[^a-z ]", "").split(" ");
        Arrays.sort(words);

        Map indexMap = new HashMap<String, List<String>>();

        for(char c = 'a'; c <= 'z'; c++){
            if(in.contains(String.valueOf(c))){
                indexMap.put(c, new ArrayList<String>());
            }
        }

        for(Object key : indexMap.keySet()){
            for(String word : words){
                if (word.contains(key.toString())) {
                    if(!((ArrayList)indexMap.get(key)).contains(word)){
                        ((ArrayList) indexMap.get(key)).add(word);
                    }
                }
            }
        }

        return indexMap;
    }

    private static void printMap(Map map){

        for(Object key : map.keySet()){
            System.out.print(key + " :");
            List list = (List)map.get(key);

            for(Object o : list){
                System.out.print(o + ", ");
            }

            System.out.println();
        }
    }
}
